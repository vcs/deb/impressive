impressive (0.13.1-1) unstable; urgency=medium

  * Fresh upstream release
    - remove debian/patches/up_crash_fix which is upstreamed
  * d/copyright
    - finally added copyright for myself and fixed debian files listing

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 21 Mar 2022 08:56:04 -0400

impressive (0.13.0~beta2-2) unstable; urgency=medium

  * debian/patches/up_crash_fix
    - upstream fix for a crash issue reported outside of BTS
  * debian/{control,compat} -- boost to 10

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 12 Jan 2022 14:35:35 -0500

impressive (0.13.0~beta2-1) unstable; urgency=medium

  * Fresh upstream beta:
    - should provide better Python3 compatibility (Closes: #961707)
  * debian/copyright
    - machine readable
    - exclude "impressive" binary shipped by upstream now
  * debian/watch
    - no longer use git-import-orig

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 02 Jun 2020 15:20:13 -0400

impressive (0.13.0~beta1a-1) unstable; urgency=medium

  * Upstream beta release
    - python3 compatible, so all depends switched to python3- counterparts
      (Closes: #956337, #912518)
  * Fixed up (B-)Depends to fix typo in xpoppler-utils (there should be no x).
    Thanks Nick for the report

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 23 Apr 2020 17:32:56 -0400

impressive (0.12.0-2) unstable; urgency=medium

  * Remove hard dependency on pdftk, keep it only in Suggests since
    impressive could still make use of it at times (Closes: #893704)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 18 Apr 2018 10:28:56 -0400

impressive (0.12.0-1) unstable; urgency=medium

  * New upstream release
    - fixes an issue with automated presentations
    - improved support for videos
  * debian/control
    - recommend ffmpeg to be used to generate video thumbnails

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 05 Feb 2018 09:58:52 -0500

impressive (0.11.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update the build dependency from python-imaging to python-pil,
    also drop the alternative dependency on python-imaging.
    (Closes: #866433)

 -- Adrian Bunk <bunk@debian.org>  Sat, 03 Feb 2018 14:41:34 +0200

impressive (0.11.2-1) unstable; urgency=medium

  * Fresh upstream release
  * debian/control
    - policy boost to 4.1.2
    - compat boost to 9
    - mupdf-tools (>= 1.5) is the primary recommended renderer now
    - removed mentioning of long obsolete xpdf-tools

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 19 Dec 2017 12:40:40 -0500

impressive (0.11.1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Update the build dependency from latex-beamer
    to texlive-latex-recommended. (Closes: #867092)

 -- Adrian Bunk <bunk@debian.org>  Tue, 15 Aug 2017 13:01:55 +0300

impressive (0.11.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add B-D: texlive-latex-extra to fix a FTBFS (Closes: #852911).

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 04 Feb 2017 14:59:00 +0500

impressive (0.11.1-1) unstable; urgency=medium

  * Recent upstream release
    - should resolve compatibility with PIL 3.0 issue (Closes:#808212)
  * debian/control
    - untabified *Depends
    - boosted policy to 3.9.6
    - no need in {python:Depends} and no need to build anything pythonish
      in debian/rules
    - source format 3.0 (quilt) now
  * debian/patches/up_overscan - added '=' in option spec. Thanks Jim Paris
    (Closes: #706122)
  * lintian override to not demand python-support

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 17 Dec 2015 21:53:47 -0500

impressive (0.10.5-1) unstable; urgency=medium

  * Fresh upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 02 May 2014 09:32:33 -0400

impressive (0.10.3+svn61-2) unstable; urgency=low

  [ Hideki Yamane ]
  * debian/control
    - add "Build-Depends: texlive-pictures" to fix FTBFS (Closes: #746144)

  [ Yaroslav Halchenko ]
  * Move perl (used by 3rd party impressive-gettransitions) to Recommends
    from Depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 01 May 2014 09:48:52 -0400

impressive (0.10.3+svn61-1) unstable; urgency=low

  * Fresh upstream snapshot
    - new --spot-radius, --clock cmdline options
    - half-screen mode, partially addresses #551778
    - various fixes
    - compatibility with pdftk 1.45 and PIL (Pillow fork)
  * Refreshing the packaging
    - migrated to dh7
    - no pysupport needed
    - dropping transitional keyjnote package and some obsolete
      alternative dependecies
    - adding xvfb, xauth, python-opengl, python-pygame, python-imaging, and
      libgl1-mesa-dri to Build-Depends to facilitate build-time smoke testing
    - boosted policy to 3.9.4 (no further changes)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 18 May 2013 13:00:52 -0400

impressive (0.10.3-2) unstable; urgency=low

  * Upload to unstable

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 10 Feb 2012 19:39:17 -0500

impressive (0.10.3-1) experimental; urgency=low

  * New upstream release.
  * [22e12d6] Move ghostscript (remove -x) into Suggests from Recommends
    (Closes: #599650)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 31 Oct 2010 22:52:43 -0400

impressive (0.10.3~WIP+svn31-1) unstable; urgency=low

  * Fresh snapshot from upstream SVN (revision 31), fixing issue with audio.

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 16 Apr 2010 08:36:32 -0400

impressive (0.10.3~WIP+svn30-1) unstable; urgency=low

  * Fresh snapshot from upstream SVN (revision 30), which includes new
    features and few bug fixes:
     - Adopted patch from Junichi Uekawa (Closes: #507877). Thanks Junichi!
     - Fixed reloading issue (Closes: #509243). Thanks Junichi for reporting.
     - Fixed a bunch of problems related to non-standard aspect ratios 
       (Closes: #510625, #572995).
       Thanks Benoit Triquet for a tentative patch for 510625.
     - Fixes PDF hyperlinks parsing (Closes: #572969)
     - Stable caption handling (Closes: #576292)
  * Adapt for migration of pdftoppm from xpdf-reader to xpdf-utils in
    3.02-2 version (Closes: #565799).

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 06 Apr 2010 18:06:08 -0400

impressive (0.10.2r-6) unstable; urgency=low

  * Abandoning X[SB]-P-V since they aren't really required if all Python
    versions are supported.

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 22 Feb 2010 22:16:36 -0500

impressive (0.10.2r-5) unstable; urgency=low

  * Fix "XS-P-V: current and Python 2.6 as default", adopted >= 2.4 value for
    XS-Python-Version. Thanks Sandro Tosi for getting Debian ready for switch
    to 2.6 (Closes: #570584).
  * boosting policy to 3.8.3 -- no changes

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 19 Feb 2010 17:27:14 -0500

impressive (0.10.2r-4) unstable; urgency=low

  * Lowering priority for transition package keyjnote from optional to extra
    (should make ftpmaster happier since it is extra already in
    overrides).

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 16 Jan 2010 10:56:15 -0500

impressive (0.10.2r-3) unstable; urgency=low

  [ Tanguy Ortolo ]
  * BF: improved manpage for impressive-gettransitions (Closes: #564757)
  * BF: fixing manpage generation under UTF8 environment (closes:
    #564766)

  [ Yaroslav Halchenko ]
  * debian/control
    - adding ${misc:Depends}
    - boosting policy to 3.8.3 -- no changes
  * debian/copyright
    - referencing versioned GPL-2

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 16 Jan 2010 09:49:08 -0500

impressive (0.10.2r-2) unstable; urgency=low

  * Resolving problems which forbid impressive to pass through NEW:
    - fixing debian/copyright to match upstream: GPLv2 firm
    - adding the source for the demo.pdf
    - added missing copyright holders into debian/copyright
    - building demo.pdf from the shipped with debian .tex source

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 05 Dec 2008 12:57:16 -0500

impressive (0.10.2r-1) unstable; urgency=low

  * New Upstream Version (version suffix 'r' stands for 'r'enamed to
    provide proper Replaces):
    - Project was renamed to impressive
    - Manpage is shipped by the upstream (closes: #482581)
  * Migrated VCS for the packaging into git, added Vcs fields to
    debian/control.
  * Boosted policy to 3.8.0 (no changes needed)
  * Removed obsolete manpage since upstream ships manpage now
  * Changed gs -> ghostscript-x in Recommends
  * Created transitional package keyjnote

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 23 Nov 2008 19:50:31 -0500

keyjnote (0.10.2-1) unstable; urgency=low

  * New upstream release:
   - uses xrandr to determine the current resolution (closes: #448091)
   - 0.10.2 version follows the standard (closes: #457551)
  * Boosted standards version to 3.7.3 (no changes needed)
  * keyjnote-gettransitions got fresh upstream version
  * Bye bye dpatch (and I like no lintian complaints ;-))

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 05 Feb 2008 13:41:55 -0500

keyjnote (0.10.1a-1) unstable; urgency=low

  * New upstream release to fix issues with altered behavior of pdftoppm
  * Homepage is a control field now

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 27 Nov 2007 18:34:21 -0500

keyjnote (0.10.1-2) unstable; urgency=low

  * Mention OpenGL in the description (Closes: #446552)

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 15 Oct 2007 13:50:24 -0400

keyjnote (0.10.1-1) unstable; urgency=low

  * New upstream release
  * Maintainership switched over to me per conversation
    with original maintainer loong time ago but without official
    wnpp/RFA request
  * Following recommendations taken from upstream author
    (Thanks Martin Fiedler):
   - pdftk is Recommended instead of Suggested now
   - keyjnote uses by default pdftoppm (added depends on
     poppler-utils | xpdf-reader) and gs can be as a fallback
     so gs moved into recommends
   - install demo.pdf

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 10 Sep 2007 11:42:43 -0400

keyjnote (0.10.0-1) unstable; urgency=low

  *  New upstream release (thanks to Vincent Bernat for the alarm email
     closes: #433019)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 14 Jul 2007 02:49:26 -0400

keyjnote (0.9.4-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 15 Apr 2007 00:57:27 -0400

keyjnote (0.9.3-1) unstable; urgency=low

  * New upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 20 Mar 2007 00:15:08 -0400

keyjnote (0.9.1-1) unstable; urgency=low

  * New upstream release (Closes: #408146, #408129)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 25 Jan 2007 08:36:38 -0500

keyjnote (0.9.0-1) unstable; urgency=low

  * New upstream release
  * Added dpatch code to rules and debian/patches directory to handle patches
    to be applied/sent upstream
  * Resolved issue with crashing in FadeOut mode whenever mouse gets pressed
    (Closes: #408133)
  * Added a script keyjnote-gettranslations (and a corresponding man
    page) useful for LaTeX users to generate .info files from the
    original LaTeX sources of the presentation
  * Updated keyjnote man page to inform about 'w'/'b' shortcuts
  * Suggests latex-beamer
  * Moved /usr/bin/keyjnote to /usr/share/keyjnote/keyjnote.py, and linked
    it back. That is so keyjnote could be used easily as a python module
    by other software (e.g. keyjnotegui which was ITPed by me)
    - Boosted-up dependency version of python-support to correspond current
      state of python packaging HOWTO
    - Added X{B,S}-Python-Version fields to debian/control
    - Moved dh_pysupport in the proper location in the sequence to generate
      pre/post scripts

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 23 Jan 2007 13:28:46 -0500

keyjnote (0.8.3-1) unstable; urgency=low

  * New upstream release (Closes: #402180)
  * Added shipped upstream keyjnote.html to be installed as doc
  * Made creation of the man page a build dependency
  * Silent lintian about character in man page by substituting it with groff
    sequence

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri,  5 Jan 2007 10:08:12 -0500

keyjnote (0.8.2-2) unstable; urgency=low

  * Minor debian/control adjustments:
    - Boosted standards-version (no changes necessary)
    - Myself in Uploaders
    - Additional feature in description
    - Trailing space removed
  * Created debian/watch for uscan

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri,  5 Jan 2007 10:05:44 -0500

keyjnote (0.8.2-1) unstable; urgency=low

  * Initial release. (Closes: #373246)

 -- Florian Ragwitz <rafl@debian.org>  Mon, 11 Sep 2006 00:32:19 +0200
